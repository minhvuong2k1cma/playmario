﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Finish : MonoBehaviour
{
    private AudioSource finish;
    [SerializeField] private GameObject winScreen;
    bool isEnded;

    // Start is called before the first frame update
    void Start()
    {
        finish = GetComponent<AudioSource>();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player" && !isEnded)
        {   
            isEnded = true;
            finish.Play();
            winScreen.SetActive(true);

            // Trong vòng 2 giây mới chạy method có tên kia
            //Invoke("NextLevel", 2f);
           
        }
        
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
