using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerCollectItem : MonoBehaviour
{
    private int m_score;
    [SerializeField] private Text scoreText;
    [SerializeField] private AudioSource collectSoundEffect;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Fruits"))
        {
            //float xSpawn = collision.gameObject.transform.position.x + Random.Range(-7, 7);
            //float ySpawn = collision.gameObject.transform.position.y + Random.Range(-7, 7);
            //Vector2 newSpawnPos = new Vector2(xSpawn,ySpawn );
            //Instantiate(collision.gameObject, newSpawnPos,Quaternion.identity);
            collectSoundEffect.Play();
            SetScoreText(++m_score);
            Destroy(collision.gameObject);
        }
    }

  

    public void IncreaseScore()
    {
        m_score++;
    }

    public void SetScoreText(int score)
    {
        scoreText.text = "Score : " +  score;
    }
}
