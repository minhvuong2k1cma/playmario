using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float maxHealth;
    [SerializeField] Slider playerHealthSlider;
    [SerializeField] float damega;
    float currentHealth;

    private Animator animator;
    private Rigidbody2D rg2D;
   // [SerializeField] AudioSource dieSoundEffect;

    void Start()
    {
        currentHealth = maxHealth;
        playerHealthSlider.maxValue = maxHealth;
        playerHealthSlider.value = maxHealth;
        animator = GetComponent<Animator>();
        rg2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap"))
        {
            currentHealth -= damega;
            playerHealthSlider.value = currentHealth;
            Debug.Log("health : " + currentHealth);
        }
        if (collision.gameObject.CompareTag("DeathZone"))
        {
            playerHealthSlider.value = 0;
            Die();
        }
    }

    private void Die()
    {
        animator.SetTrigger("death");
        rg2D.bodyType = RigidbodyType2D.Static;
        //dieSoundEffect.Play();
        ResetLevel();
    }

    private void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
}
