using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
            Instance = this;
    }
    private Rigidbody2D rg2D;
    private float xDirection;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D boxCollider2D;

    [SerializeField] private float forcePower;
    [SerializeField] private float moveSpeed;
    [SerializeField] private LayerMask jumpAbleGround;
    [SerializeField] private AudioSource jumpSoundEffect;

    private Animator animator;


    // Start is called before the first frame update

    void Start()
    {
        rg2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {
        bool isJump = (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.UpArrow)) && OnGrounded();
        if (isJump)
        {
            rg2D.velocity = new Vector2(rg2D.velocity.x, forcePower);
            jumpSoundEffect.Play();
        }
        xDirection = Input.GetAxisRaw("Horizontal");
        rg2D.velocity = new Vector2(moveSpeed * xDirection, rg2D.velocity.y);
        SetPlayerState();


    }

    private void SetPlayerState()
    {
        if (rg2D.velocity.x < 0)
        {
            animator.SetInteger("state", 1);
            spriteRenderer.flipX= true;
        }else if (rg2D.velocity.x > 0)
        {
            animator.SetInteger("state", 1);
            spriteRenderer.flipX = false;


        }
        else
        {
            animator.SetInteger("state", 0);
        }
        if (rg2D.velocity.y > .1f)
        {
            animator.SetInteger("state", 2);

        }else if (rg2D.velocity.y < -.1f)
        {
            animator.SetInteger("state", 3);

        }




    }

    private bool OnGrounded()
    {
        return Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, .1f, jumpAbleGround);
    }
}
