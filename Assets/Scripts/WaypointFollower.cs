using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour
{
    [SerializeField] private Transform[] wayPoints;
    [SerializeField] private float moveSpeed;


    private int currentWayPointIndex = 0;

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(wayPoints[currentWayPointIndex].position, transform.position) < .1f) 
        {
            currentWayPointIndex++;
            if(currentWayPointIndex>= wayPoints.Length)
            {
                currentWayPointIndex = 0;
            }
        }
        transform.position = Vector2.MoveTowards(transform.position, wayPoints[currentWayPointIndex].position, Time.deltaTime*moveSpeed);
    }
}
