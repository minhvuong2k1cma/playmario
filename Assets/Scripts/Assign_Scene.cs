using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Assign_Scene : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float moveSpeed;
    private Rigidbody2D rg2D;
    void Start()
    {
        rg2D= GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
      float xDirection =  Input.GetAxisRaw("Horizontal");
       
        if (xDirection < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);

            // Dampen towards the target rotation
        }else if (xDirection > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);

        }
        rg2D.velocity = new Vector2(xDirection * moveSpeed, rg2D.velocity.y);
    }
}
