using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    public float speed = 5f;
    Vector2 moveDirection;
    private Rigidbody2D rg_2D;
    // Start is called before the first frame update
    private void Awake()
    {
        rg_2D = this.GetComponent<Rigidbody2D>();

   
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {


        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rg_2D.rotation = angle;
        direction.Normalize();
        moveDirection = direction;
    }
    private void FixedUpdate()
    {
        MoveCharacter(moveDirection);
        //transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);

    }

    void MoveCharacter(Vector2 direction)
    {
        rg_2D.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }
}
